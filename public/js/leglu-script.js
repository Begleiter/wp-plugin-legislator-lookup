/**
 * This is the primary script for the
 * Legislator Lookup Plugin
 *
 * @summary   script for Legislator Lookup Plugin
 *
 * @link      https://goodgoodwork.io/plugins/legislator-lookup
 * @since     2017.06.06
 * @requires jquery.js
 * @class
 * @classdesc This is a description of the MyClass class.
 */

( function( $ ) {
  var autocomplete, place,

      searchHistory = [],
      settings = leglu_settings,
      addressInput = $( '#' + settings.inputAddressID );

  // Activate autocomplete
  autocomplete = new google.maps.places.Autocomplete(
    (document.getElementById( settings.inputAddressID )),
    {types: ['geocode']});

  // Listen for place to change
  autocomplete.addListener('place_changed', setPlace);

  // on click look up place
  $( '#' + settings.inputSubmitID ).click(function(e) {
    e.preventDefault();

    var data;
    setPlace();
    prepareData( function( address, lat, lng ){
      if ( address ) {
        data = {
          action: 'leglu',
          nonce: settings.nonce,
          address: address,
          lat: lat,
          lng: lng
        };
        addressLookup( data );
      }
    });
  });

  /**
   * Set the place variable
   * @return {bool} false if no address value present in address input
   */
  function setPlace() {
    var addressVal = addressInput.attr('value');
    // exit if address input is blank
    if ( addressVal === '' ){
      place = false;
      return false;
    }

    place = autocomplete.getPlace();

    if ( typeof place === 'undefined' ){
      //set place to address input value
      place = { name: addressVal };
    }
  }

  /**
   * prepares the data for ajax call
   * @param  {Function} callback returns:
   *         {string} formatted address (or false if there is no place data)
   *         {string} latitude of address
   *         {string} longitude of address
   * @return {void}
   */
  function prepareData( callback ){
    if ( ! place ) {
      // if place is false abort
      callback( false );
      // if place has an id we know it also has lat/lng
    } else if ( place.id ) {
      callback(
        place.formatted_address,
        place.geometry.location.lat(),
        place.geometry.location.lng()
      );

    } else {
      // if there is no place id we need to geocode the address
      var geocoder = new google.maps.Geocoder();

      geocoder.geocode( {'address': place.name}, function(results, status) {
        if (status === 'OK') {
          // TODO confirm found address
          setAddressValue(results[0].formatted_address);
          callback(
            results[0].formatted_address,
            results[0].geometry.location.lat(),
            results[0].geometry.location.lng()
          );
        } else {
          // TODO throw error if geocode fails
          console.log('Geocode was not successful for the following reason: ' + status);
          callback( false );
        }
      });
    }
  }

  /**
   * Set the value attribute for the address input field
   * @param {string} val the content to set for the input value
   */
  function setAddressValue( val ){
    addressInput.attr('value',val);
  }

  /**
   * Sends the ajax call to lookup address
   * @param  {object} data object containing data for ajax call
   * @return {undefined}      no return, simply acts on success or failure
   */
  function addressLookup( data ){
    $.ajax({
      url : settings.ajax_url,
      dataType : 'json',
      type : 'post',
      data : data,
      beforeSend : function() {
        $('#leglu-input-spinner').css('display', 'inline')
      },
      complete : function() {
        $('#leglu-input-spinner').css('display', 'none')
      },
      success : function( response ) {
        // TODO break out success into function
        var $wrapper = $( '#' + settings.renderWrapperID );
        if (searchHistory.length > 0) {
          // if there is a search history we can assume there a response visible
          // remove the first child, assumed to be the response html from last time
          $wrapper.children()[0].remove();
        }
        searchHistory.push(response);

        $wrapper.show().prepend( response.html );
        initAccordion();
      },
      error: function(errorThrown){
        console.log('error:', errorThrown);
      }
    });
  }

  /**
   * initiate the accordion Functionality for the list of representatives
   * @return {void} returns nothing
   */
  function initAccordion(){
    var activeClass = 'leglu--active';
        allPanels = $('.accordion dl > dd').hide();

    $('.accordion dl > dt').click(function() {
      var $this = $(this);
          $container = $(this).next();

      allPanels.slideUp();
      if ($this.hasClass(activeClass)) {
        $this.removeClass(activeClass);
        return false;
      } else {
        $('.accordion dl > dt').removeClass(activeClass);
        $container.slideDown();
        $this.addClass(activeClass);
        return false;
      }
    });
  }
} )( jQuery );
