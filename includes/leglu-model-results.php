<?php

/**
 * Legislator Lookup results model
 *
 * @package Legislator Lookup
 * @subpackage Legislator Lookup Model Results
 * @since 2017.06.06
 */
class LegLuModelResults
{
  private $apiData;
  /**
   * class constructor
   */
  public function __construct( $apiData )
  {
    $this->apiData = json_decode( $apiData );
  }
}
