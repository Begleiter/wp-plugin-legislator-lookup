<?php

/**
 * Legislator Lookup input model
 *
 * @package Legislator Lookup
 * @subpackage Legislator Lookup Model Input
 * @since 2017.06.06
 */
class LegLuModelInput
{
  private $addressPlaceholder;
  private $inputSubmitValue;

  /**
   * class constructor
   */
  public function __construct()
  {
    $this->addressPlaceholder = __('Enter your address here','leglu');
    $this->inputSubmitValue = __('Lookup','leglu');
  }

  public function getData(){
    return array(
      'placeholder' => $this->addressPlaceholder,
      'submitValue' => $this->inputSubmitValue
    );
  }

  /**
   * returns address input placeholder text
   * @return string text for address input placeholder
   */
  public function getAddressPlaceholder(){
    return $this->addressPlaceholder;
  }
  /**
   * returns address input placeholder text
   */
  public function setAddressPlaceholder($string){
    $this->addressPlaceholder = $string;
  }

  /**
   * returns input submit text
   * @return string text for input submit value
   */
  public function getInputSubmitValue(){
    return $this->inputSubmitValue;
  }
  /**
   * returns input submit value
   */
  public function setInputSubmitValue($string){
    $this->inputSubmitValue = $string;
  }
}
