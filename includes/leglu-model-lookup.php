<?php

/**
 * Legislator Lookup API model
 *
 * @package Legislator Lookup
 * @subpackage Legislator Lookup Model Lookup
 * @since 2017.06.06
 */
class LegLuModelLookup
{
  private $apiKey;
  private $apiEndpoint = 'https://openstates.org/api/v1/';

  const TIMEOUT = 10;

  /**
   * class constructor
   */
  public function __construct( $apiKey, $apiEndpoint = null )
  {
    $this->apiKey = $apiKey;

    if( null !== $apiEndpoint ) {
      $this->apiEndpoint = $apiEndpoint;
    }
  }

  public function url( $method, $params = null){
    if ( null !== $params) {
      $params = '?' . http_build_query( $params );
    }
    return $this->apiEndpoint . ltrim( $method, '/' ) . $params;
  }

  public function legislatorGeoLookup( $lat, $lng )
  {
    if( $lat && $lng ){

      $params['apikey'] = $this->apiKey;
      $params['lat']    = $lat;
      $params['long']   = $lng;

      $url = $this->url( 'legislators/geo/', $params );

      return wp_remote_get( $url );
    }

    return false;
  }
}
