<?php
/**
 * Legislator Lookup Results View
 *
 * Constructs HTML for display of lookup input form
 * @package Legislator Lookup
 * @since 2017.06.06
 */

/**
 * class to render lookup results
 *
 * @package Legislator Lookup
 * @since 2017.06.06
 */
class LegLuViewResults
{
  /**
   * renders HTML
   * @param  array $data associative array of data for HTML
   * @return string       the HTML output
   */
  public static function render( $data ){
    $data = array( 'representatives' => json_decode($data) );
    $m = new Mustache_Engine;
    $data['text'] = array(
      'district' => __('district','leglu'),
      'picture_of' => __('Picture of','leglu'),
      'visit_website' => __('Website link','leglu'),
      'call_rep' => __('Phone','leglu'),
      'email_rep' => __('Email','leglu'),
    );
    ob_start();
    // TODO handle missing or empty data
?>
    <dl class="leglu-result-wrapper">
      {{# representatives }}
      <dt class="leglu-rep--{{ party }} leglu-rep__header">
        <strong class="leglu-rep__full-name">{{ full_name }}</strong>
        <br>
        <span class="leglu-rep__chamber">{{ chamber }}</span>
        <span class="leglu-rep__district">{{ text.district }} {{ district }}</span>
        <span class="leglu-rep__party">{{ party }}</span>
      </dt>
      <dd class="leglu-rep__content">
        <div class="leglu-rep__col-left">
          {{# photo_url }}
          <img src="{{ photo_url }}" alt="{{ text.picture_of }} {{ full_name }}">
          <br>
          {{/ photo_url }}
          {{# url }}
          <a href="{{ url }}" target="_blank">{{ text.visit_website }}</a>
          {{/ url }}
        </div>
        <div class="leglu-rep__col-right leglu-rep-contact">
          {{# offices }}
          <div class="leglu-rep-office__{{ type }}">
            <p>
              <strong class="leglu-rep-office__name">{{ name }}:</strong>
            </p>
            {{# address }}
            <p class="leglu-rep-office__address">{{ address }}</p>
            {{/ address }}
            {{# phone }}
            <p class="leglu-rep-office__phone"><a href="tel:{{ phone }}" target="_blank">{{ text.call_rep }}:</a> <span>{{ phone }}</span></p>
            {{/ phone }}
            {{# email }}
            <p class="leglu-rep-office__email"><a href="mailto:{{ email }}" target="_blank">{{ text.email_rep }}:</a> <span>{{ email }}</span></p>
            {{/ email }}
          </div>
          {{/ offices }}
        </div>
      </dd>
      {{/ representatives }}
    </dl>
<?php
    return $m->render(ob_get_clean(), $data );
  }
}
